'use strict';

const gulp = require('gulp');
const browserSync = require('browser-sync').create();

const scss = require('gulp-sass');
const prefix = require('gulp-autoprefixer');
const sourcemap = require('gulp-sourcemaps');
const imagemin = require('gulp-imagemin');
const rename = require('gulp-rename');
const cache = require('gulp-cache');
const png = require('imagemin-pngquant');
const gulpIf = require('gulp-if');
const del = require('del');

const isDevelopment = !process.env.NODE_ENV || process.env.NODE_ENV == 'development';

let path = {
    src: {
        scss: 'src/css/scss/**/*.scss',
        image: 'src/img/*.*'
    },
    local: {
        scss: 'src/css/',
        image: 'src/img/min'
    },
    prod: {
        css: 'prod/css/',
        img: 'prod/img/',
        js: 'prod/js/',
        html: 'prod/'
    }
};

/**
 * Browser Sync
 */
gulp.task('serve', function() { 
    browserSync.init({
        server: {
            baseDir: 'src/' 
        },
        notify: false
    });
    gulp.watch("src/css/scss/*.scss", gulp.series('scss'));
    gulp.watch("src/img/**/*.*", gulp.series('image'));
    gulp.watch("src/*.html").on('change', browserSync.reload);
});


/**
 * Scss
 */
gulp.task('scss', function(){
    return gulp.src(path.src.scss)
    .pipe(gulpIf(isDevelopment, sourcemap.init()))
    .pipe(prefix({
        browsers: ['last 10 versions', '> 1%']
    }))
    .pipe(scss({ outputStyle: 'expanded' }).on('error', scss.logError))
    .pipe(gulpIf(isDevelopment, sourcemap.write()))
    .pipe(gulp.dest(path.local.scss))
    .pipe(browserSync.stream())
});

/**
 * Minify Image
 */
let index = 1;
gulp.task('image', function(){
    return gulp.src(path.src.image)
    .pipe(cache(imagemin({
        interlaced: true,
        progressive: true,
        svgoPlugins: [{removeViewBox: false}],
        use: [png()]
    })))
    .pipe(rename(function(path){
        path.basename = ("img-" + index++)
    }))
    .pipe(gulp.dest(path.local.image))
});


/**
 * Builder
 */
gulp.task('prebuild', async function(){
    let buildCss = gulp.src('src/css/style.css')
        .pipe(gulp.dest(path.prod.css))
    
    let buildImg = gulp.src('src/img/min/*.*')
        .pipe(gulp.dest(path.prod.img))

    let buildJs = gulp.src('src/js/*.*')
        .pipe(gulp.dest(path.prod.js))
    
    let buildHtml = gulp.src('src/*.html')
        .pipe(gulp.dest(path.prod.html))
});

/**
 * Удаляем папку production
 */
gulp.task('clean', function(){
    return del('prod');
});


gulp.task('build', gulp.parallel('prebuild', 'clean', 'image', 'scss'));